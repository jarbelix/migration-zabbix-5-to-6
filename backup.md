# Dump Database Zabbix 5

```bash
IP=`hostname --all-ip-addresses | awk '{ print $1 }'`

DOW=`date +%a`

DB='zabbix'

_USER="root"
_PWD="root-password"
_DUMP_DIR="/root/mysqldump"
_LOCAL="$_DUMP_DIR/mysql-$IP-$DOW-$DB"

mysqldump $DB --user="$_USER" --password="$_PWD" --add-drop-database --skip-set-charset --default-character-set=utf8mb4 --skip-lock-tables > $_LOCAL

gzip -f $_LOCAL
```

## Listing Backup

```bash
ls -l /root/mysqldump/
total 16238216
-rw-r--r-- 1 root root 2380403805 Mar  4 02:10 mysql-192.168.1.1-Fri-zabbix.gz
-rw-r--r-- 1 root root 2354986197 Feb 28 02:11 mysql-192.168.1.1-Mon-zabbix.gz
-rw-r--r-- 1 root root 2385186765 Mar  5 02:11 mysql-192.168.1.1-Sat-zabbix.gz
-rw-r--r-- 1 root root 2393241428 Mar  6 02:11 mysql-192.168.1.1-Sun-zabbix.gz
-rw-r--r-- 1 root root 2377318957 Mar  3 02:11 mysql-192.168.1.1-Thu-zabbix.gz
-rw-r--r-- 1 root root 2366017524 Mar  1 02:12 mysql-192.168.1.1-Tue-zabbix.gz
-rw-r--r-- 1 root root 2370737920 Mar  2 02:11 mysql-192.168.1.1-Wed-zabbix.gz
root@s256:~# 
```

## Copy Backup to New Server

```bash
rsync -av /root/mysqldump/ root@NEW-SERVER:/root/mysqldump/
```
