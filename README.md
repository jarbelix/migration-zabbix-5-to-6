# Dump, Import, Convert Database Zabbix 5 to 6

## See More:

1.	https://youtu.be/dvmLynvsxOI (Zabbix 6.0 Upgrade - Database Requirements)
1.	https://www.zabbix.com/documentation/6.0/en/manual/installation/requirements
1.	https://youtu.be/WXBl512QnX8?t=1011 (Zabbix Upgrade To 6.0 LTS - Dmitry Lambert)
1.	https://www.zabbix.com/documentation/6.0/en/manual/appendix/install/db_primary_keys#mysql

## Dump

Howto [Export Database Zabbix](../backup.md)

## Import

Howto [Restore Database Zabbix](../restore.md)

