# Import Backup to Zabbix 6

```bash
IP=`hostname --all-ip-addresses | awk '{ print $1 }'`

DOW=`date +%a`

DB='zabbix'

_DUMP_DIR="/root/mysqldump"
_LOCAL="$_DUMP_DIR/mysql-$IP-$DOW-$DB"

mysql << EOF
create database zabbix character set utf8mb4 collate utf8mb4_bin;
create user zabbix@localhost identified by 'password';
grant all privileges on zabbix.* to zabbix@localhost;
EOF

DB_BACKUP=$_LOCAL.gz

zcat $DB_BACKUP | mysql zabbix

HISTORY_PK_PREPARE='/usr/share/doc/zabbix-sql-scripts/mysql/history_pk_prepare.sql'

SECURE_FILE_PRIV='/var/lib/mysql-files'

{

echo "Rename history* tables to _old and Create new Tables - `date`"
mysql zabbix < $HISTORY_PK_PREPARE

echo "Export/Import TABLE history - `date`"
mysql zabbix << EOF-history
SET @@max_execution_time=0;
SELECT * INTO OUTFILE '$SECURE_FILE_PRIV/history.csv' FIELDS TERMINATED BY ',' ESCAPED BY '"' LINES TERMINATED BY '\n' FROM history_old;
DROP TABLE history_old;
LOAD DATA INFILE '$SECURE_FILE_PRIV/history.csv' IGNORE INTO TABLE history FIELDS TERMINATED BY ',' ESCAPED BY '"' LINES TERMINATED BY '\n';
EOF-history
rm -f $SECURE_FILE_PRIV/history.csv

echo "Export/Import TABLE history_uint - `date`"
mysql zabbix << EOF-history_uint
SET @@max_execution_time=0;
SELECT * INTO OUTFILE '$SECURE_FILE_PRIV/history_uint.csv' FIELDS TERMINATED BY ',' ESCAPED BY '"' LINES TERMINATED BY '\n' FROM history_uint_old;
DROP TABLE history_uint_old;
LOAD DATA INFILE '$SECURE_FILE_PRIV/history_uint.csv' IGNORE INTO TABLE history_uint FIELDS TERMINATED BY ',' ESCAPED BY '"' LINES TERMINATED BY '\n';
EOF-history_uint
rm -f $SECURE_FILE_PRIV/history_uint.csv

echo "Export/Import TABLE history_str - `date`"
mysql zabbix << EOF-history_str
SET @@max_execution_time=0;
SELECT * INTO OUTFILE '$SECURE_FILE_PRIV/history_str.csv' FIELDS TERMINATED BY ',' ESCAPED BY '"' LINES TERMINATED BY '\n' FROM history_str_old;
DROP TABLE history_str_old;
LOAD DATA INFILE '$SECURE_FILE_PRIV/history_str.csv' IGNORE INTO TABLE history_str FIELDS TERMINATED BY ',' ESCAPED BY '"' LINES TERMINATED BY '\n';
EOF-history_str
rm -f $SECURE_FILE_PRIV/history_str.csv

echo "Export/Import TABLE history_log - `date`"
mysql zabbix << EOF-history_log
SET @@max_execution_time=0;
SELECT * INTO OUTFILE '$SECURE_FILE_PRIV/history_log.csv' FIELDS TERMINATED BY ',' ESCAPED BY '"' LINES TERMINATED BY '\n' FROM history_log_old;
DROP TABLE history_log_old;
LOAD DATA INFILE '$SECURE_FILE_PRIV/history_log.csv' IGNORE INTO TABLE history_log FIELDS TERMINATED BY ',' ESCAPED BY '"' LINES TERMINATED BY '\n';
EOF-history_log
rm -f $SECURE_FILE_PRIV/history_log.csv

echo "Export/Import TABLE history_text - `date`"
mysql zabbix << EOF-history_text
SET @@max_execution_time=0;
SELECT * INTO OUTFILE '$SECURE_FILE_PRIV/history_text.csv' FIELDS TERMINATED BY ',' ESCAPED BY '"' LINES TERMINATED BY '\n' FROM history_text_old;
DROP TABLE history_text_old;
LOAD DATA INFILE '$SECURE_FILE_PRIV/history_text.csv' IGNORE INTO TABLE history_text FIELDS TERMINATED BY ',' ESCAPED BY '"' LINES TERMINATED BY '\n';
EOF-history_text
rm -f $SECURE_FILE_PRIV/history_text.csv

}

```
